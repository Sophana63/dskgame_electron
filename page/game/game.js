const {ipcRenderer} = require('electron');
let body = document.querySelector('body');
let divPoints = document.getElementById('points');
let divCombos = document.getElementById('combos');
const point = 100; 
let points = 0;
let combo = 1;
let count = 60;


function randomIntFromInterval(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min)
}


function dsk_lover(){

    let img = document.createElement("img");
    img.src = "../../assets/images/dsk.png";
    img.setAttribute('id', 'img_dsk');

    let angle=0;
    setInterval(function(){
        img.style.transform="rotateZ("+ angle++ +"deg)";
    }, 20);

    img.style.top=randomIntFromInterval(180, 600) + 'px';
    img.style.left=randomIntFromInterval(180, 1100) + 'px';

    body.appendChild(img);

    setTimeout(() => {
        img.remove();
    }, randomIntFromInterval(1000, 1500))

    setTimeout(dsk_lover, randomIntFromInterval(500, 1000));
}

body.addEventListener('click', (e) => {
    console.log(e.target.getAttribute('id'));   
    const cible =  e.target.getAttribute('id');

    let explosion = document.createElement("img");
    explosion.src = "../../assets/images/explosion.gif";
    explosion.setAttribute('id', 'img_explosion');

    if (cible !== "img_dsk") {
        combo = 1;
    }
    if (cible == "img_dsk") {       
        e.target.remove()

        explosion.style.top = e.target.style.top;
        explosion.style.left = e.target.style.left;

        body.appendChild(explosion);

        setTimeout(() => {
            explosion.remove();
        }, 810)

        console.log("x: "+ (e.target.style.top));
        console.log("y: "+ (e.target.style.left));

        points = points + (point * combo);
        combo++;
        divPoints.innerHTML = "";
        divCombos.innerHTML = "";
        divPoints.insertAdjacentHTML( 'beforeend', '<p> POINTS: ' + points + '</p>');
        divCombos.insertAdjacentHTML( 'beforeend', '<p>' + combo + ' HITS COMBOS!</p>');
        console.log(points);

    }
    
})

function myTimer() {
    count--;
    document.getElementById("timer").innerHTML = count;
    if (count == 0) {
      ipcRenderer.send('end_game', points);
    }
}

setInterval(myTimer, 1000);

dsk_lover();
    

  

