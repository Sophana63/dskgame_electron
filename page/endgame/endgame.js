const {ipcRenderer} = require('electron');
let divPoints = document.getElementById('points');
let points = 0;

ipcRenderer.on('envoi_points', (event, arg) => {
    points = arg;
    divPoints.innerHTML = "";
    divPoints.insertAdjacentHTML( 'beforeend', '<p> POINTS: ' + points + '</p>');    
})